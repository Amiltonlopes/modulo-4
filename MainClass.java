package pacote1;

public class MainClass {

    public static void main(String[] args){
        //aqui atribuimos os valores aos atributos do método construtor.
        Carro carro = new Carro(4,3,"Volkswagen", "Gol", "9BWHE21JX24060690",2015,"Gasolina Comum",true,5,4,true,true,true);
        //aqui atribuimos valor aos atributos da Classe de Origem.
        carro.setCor(Carro.Preto);
        carro.setAirBag(true);
        carro.setRevestimento("Couro");
        carro.setArCondicionado(true);
        carro.setVidroTravas(true);
        carro.setDirecaoHidraulica(true);
        carro.setCorFarois("Luz Branca");
        carro.imprimeValores();//aqui chamamos um método(); que foi criado dentro da Classe de origem para exibir os valores dos atributos.
       
        
}
   
}

