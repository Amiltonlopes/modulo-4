 package pacote1;

public class Carro {

    //Atributos da Classe.
    public static final String Vermelho = "Vermelho";
    public static final String Preto = "Preto";
    public static final String Azul = "Azul";
    private String marca;
    private String modelo;
    private String numChassi;
    private int anoFabricacao;
    private int quantidadePneus;
    private int quantidadeCalotas;
    private  int quantidadeParafusosPneu;
    private String cor;
    private boolean arCondicionado;
    private int quantBancos;
    private boolean airBag;
    private boolean cintoSeguranca;
    private int numPortas;
    private boolean freioMao;
    private int cintoSeg;
    private String revestimento;
    private boolean vidroTravas;
    private boolean direcaoHidraulica;
    private boolean paraChoques;
    private boolean farois;
    private String corFarois;
    private String combustivel;
    

    //Métodos especiais:
    //Método Construtor: somente intens (atributos) obrigatorios.
    //aqui declaramos os atributos do método construtor.
    public Carro(int quantidadePneus,int quantBancos,String marca, String modelo,String numChassi,int anoFabricacao,String combustivel,boolean cintoSeguranca,int cintoSeg,int numPortas,boolean freioMao,boolean paraChoques, boolean farois){
       setQuantidadePneus(quantidadePneus);
       setQuantBancos(quantBancos);
       setMarca(marca);//aqui o parametro de cada método setter(); vai receber o valor que entra pelo método Construc e armazenar esse valor
       setModelo(modelo);//no seu atributo corespondente.
       setCintoSeguranca(cintoSeguranca);
       setCintoSeg(cintoSeg);
       setFreioMao(cintoSeguranca);
       setParaChoques(freioMao);
       setFarois(paraChoques);
       setNumPortas(numPortas);
       setNumChassi(numChassi);
       setAnoFabricacao(anoFabricacao);
       setAnoFabricacao(anoFabricacao);
       setCombustivel(combustivel);
       
    }
    //Métodos Getter e Setter de cada atributo.

    public int getQuantidadePeneus(){
        return quantidadePneus + 2;
    }

    public void setQuantidadePneus(int quantidadePneus){

        setQuantidadeCalotas(quantidadePneus);


        setQuantidadeParafusosPneu(quantidadePneus *4);

        this.quantidadePneus = quantidadePneus;
    }
    
    public void setCor(String cor){
        this.cor = cor;

    }
    
    
    public String getCor() {
        return cor;
    }

    public int getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas(int quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public int getQuantidadeParafusosPneu() {
        return quantidadeParafusosPneu;
    }

    public void setQuantidadeParafusosPneu(int quantidadeParafusosPneu) {
        this.quantidadeParafusosPneu = quantidadeParafusosPneu;
    }

    
     public boolean getArCondicionado() {
        return arCondicionado;
    }

    public void setArCondicionado(boolean arCondicionado) {
        this.arCondicionado = arCondicionado;
    }

    

    public int getQuantBancos() {
        return quantBancos;
    }

    public void setQuantBancos(int quantBancos) {
        this.quantBancos = quantBancos;
    }

     public boolean getAirBag() {
        return airBag;
    }

    public void setAirBag(boolean airBag) {
        this.airBag = airBag;
    }
    

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    

    public boolean isCintoSeguranca() {
        return cintoSeguranca;
    }

    public void setCintoSeguranca(boolean cintoSeguranca) {
        this.cintoSeguranca = cintoSeguranca;
    }

    public int getCintoSeg() {
        return cintoSeg;
    }

    public void setCintoSeg(int cintoSeg) {
        this.cintoSeg = cintoSeg;
    }
    

    public String getRevestimento() {
        return revestimento;
    }

    public void setRevestimento(String revestimento) {
        this.revestimento = revestimento;
    }

      public boolean isVidroTravas() {
        return vidroTravas;
    }

    public void setVidroTravas(boolean vidroTravas) {
        this.vidroTravas = vidroTravas;
    }

    public void setDirecaoHidraulica(boolean direcaoHidraulica){
        this.direcaoHidraulica = direcaoHidraulica;
    }

  

  public boolean isDirecaoHidraulica(){
    return direcaoHidraulica;
  }

     public boolean isFreioMao() {
    return freioMao;
}

public void setFreioMao(boolean freioMao) {
    this.freioMao = freioMao;
}

public boolean isParaChoques() {
    return paraChoques;
}

public void setParaChoques(boolean paraChoques) {
    this.paraChoques = paraChoques;
}

public boolean isFarois() {
    return farois;
}

public void setFarois(boolean farois) {
    this.farois = farois;
}

public String getCorFarois() {
    return corFarois;
}

public void setCorFarois(String corFarois) {
    this.corFarois = corFarois;
}

  public int getNumPortas() {
    return numPortas;
}

public void setNumPortas(int numPortas) {
    this.numPortas = numPortas;
}

 public String getNumChassi() {
    return numChassi;
}

public void setNumChassi(String numChassi) {
    this.numChassi = numChassi;
}

 public int getAnoFabricacao() {
    return anoFabricacao;
}

public void setAnoFabricacao(int anoFabricacao) {
    this.anoFabricacao = anoFabricacao;
}

public void setCombustivel(String combustivel){
    this.combustivel = combustivel;
}

public String getCombustivel(){
    return this.combustivel;
}

    //Método que mostra o estado do carro, (mostra os valores atribuidos a cada atributo da classe).
     public void imprimeValores(){
        System.out.println("Um Carro: ");
        System.out.println("Marca: " + getMarca());
        System.out.println("Modelo: " + getModelo());
        System.out.println("Ano de Fabricação: " + getAnoFabricacao());
        System.out.println("Tipo de cobustivel: " + getCombustivel());
        System.out.println("Cor: " + getCor());
        System.out.println("Número do Chassi: " + getNumChassi());
        System.out.println("Quantidade de portas: " + getNumPortas());
        System.out.println("Quantidade de Pneus: " + getQuantidadePeneus());
        System.out.println("Quantidade de Calotas: " + getQuantidadeCalotas());
        System.out.println("Quantidade de Parafusos: " +getQuantidadeParafusosPneu());
        System.out.println("Quantidade de Bancos: " + getQuantBancos());
        System.out.println("Revestimento dos bancos: " + getRevestimento());
        System.out.println("O carro possue cinto de segurança? " +isCintoSeguranca());
        System.out.println("Quantos cintos de segurança o carro possue? " + getCintoSeg());
        System.out.println("O carro possue freio de mão? " + isFreioMao());
        System.out.println("O carro possue parachoques? " + isParaChoques());
        System.out.println("O carro possue farois? " + isFarois());
        System.out.println("Qual a cor dos farois? " + getCorFarois());
        System.out.println("O carro possue ar condicionado? " + getArCondicionado());
        System.out.println("O carro possue Airbag? " + getAirBag());
        System.out.println("O carro possue Vidros e travas eletricas? " + isVidroTravas());
        System.out.println("O carro tem direção Hidraulica? " + isDirecaoHidraulica());
    
    }
    
}

    

